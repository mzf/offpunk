Source: offpunk
Section: net
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Francois Mazen <francois@mzf.fr>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-setuptools
Build-Depends-Indep: python3
Standards-Version: 4.6.2
Homepage: https://sr.ht/~lioploum/offpunk/
Vcs-Git: https://salsa.debian.org/mzf/offpunk.git
Vcs-Browser: https://salsa.debian.org/mzf/offpunk
Rules-Requires-Root: no

Package: offpunk
Architecture: all
Depends: less,
         file,
         xdg-utils,
         xsel,
         chafa,
         timg,
         python3-cryptography,
         python3-requests,
         python3-feedparser,
         python3-bs4,
         python3-readability,
         python3-pil,
         python3-setproctitle,
         ${misc:Depends},
         ${python3:Depends}
Description: Offline-First command-line browser for the Smolnet
 Offpunk is a command-line browser and feed reader dedicated to browsing the
 Web, Gemini, Gopher and Spartan. Thanks to its permanent cache, it is
 optimised to be used offline with rare connections but works as well when
 connected.
 Offpunk is optimised for reading and supports readability mode, displaying
 pictures, subscribing to pages or RSS feeds, managing complex lists of
 bookmarks. Its integrated help and easy commands make it a perfect tool for
 command-line novices while power-users will be amazed by its shell integration.
